#include "headers.hpp"
#include "powers.hpp"
#include "linkedlist.hpp"
#include "Planet.hpp"
#include "gravity.hpp"
#include "collisionDetect.hpp"
#include <cstdlib>

LinkedList planetList;

int main()
{
  sf::RenderWindow window(sf::VideoMode(windowSize.x,windowSize.y),"Gravity Simulation",sf::Style::Close); //Set window resolution and title.
  //window.setFramerateLimit(120); //Limit framerate to 120fps.
  sf::Clock clock; //Records total duration of program.

  float lastTime = 0; //Time on 'clock' at previous frame.
  float circleVel = 0.f; //Circle's current velocity. Will be increased by 9.81 every second.
  srand (time(NULL));
  for (int i=0; i<300; ++i){
    planetList.Add(
		   new Planet(sf::Color(255,255,255,255),
			      ((float)rand()/RAND_MAX)*4.f + ((float)rand()/RAND_MAX)*4.f,  
			      10.f,
			      sf::Vector2f(((float)rand()/RAND_MAX)*1280,((float)rand()/RAND_MAX)*720)) //Radom Position (0-1280,0-720) = whole screen
		   );
  }


  int frameNumber = 0;
  while (window.isOpen()) //Loop for every frame while window is open. 
    {
      sf::Event event; //Events store every keypress, mouse movement, etc. in the program.
      while (window.pollEvent(event)) //Loop through each new event.
        {
	  if (event.type == sf::Event::Closed) //If the event is clicking the X, close the window.
	    window.close();
        }
      float currentTime = clock.getElapsedTime().asSeconds(); //Set to current time on the clock.
      
      applyGravity(&planetList,currentTime-lastTime);
      checkCollisions(&planetList);
      planetList.MoveAll(currentTime-lastTime);
      window.clear();
      planetList.DrawAll(&window);
      window.display(); 

      std::cout << 1.f / (currentTime - lastTime) << " fps. Frame #" << frameNumber++ << "\n";
    

      lastTime = currentTime; //Record the current time for use in the next frame.
  }
  return 0;
}
