#include "headers.hpp"
#include "Planet.hpp"
#include "linkedlist.hpp"
#include "powers.hpp"
#include "vectorMath.hpp"
#include <math.h>

void checkCollisions(LinkedList* planetList){
  node* body1 = planetList->first;
  node* body2;
  float x1,x2,y1,y2,M1,M2;
  while (body1->next != NULL){
    body2 = body1->next;
    while (body2 != NULL){
      sf::Vector2f relativePos = body2->target->getCOMPosition() - body1->target->getCOMPosition();
      float combinedRadius = body1->target->shape.getRadius() + body2->target->shape.getRadius();
      if (magnitude(relativePos) < combinedRadius){
	sf::Vector2f desiredRelativePos = normalize(relativePos) * combinedRadius - relativePos;
	float combinedMass = body1->target->getMass() + body2->target->getMass();
	sf::Vector2f relPos1 = desiredRelativePos * (body2->target->getMass() / combinedMass);
	sf::Vector2f relPos2 = desiredRelativePos * (body1->target->getMass() / combinedMass);

	body1->target->setCOMPosition(body1->target->getCOMPosition() - relPos1);
	body2->target->setCOMPosition(body2->target->getCOMPosition() + relPos2);

	sf::Vector2f n = normalize(relativePos);

	sf::Vector2f V1 = body1->target->getSpeed();
	sf::Vector2f V2 = body2->target->getSpeed();

        float a1 = dot(V1,n);
	float a2 = dot(V2,n);

	float optimisedP = (2.0f * (a1-a2)) / (body1->target->getMass() + body2->target->getMass());

	sf::Vector2f newV1 = V1- optimisedP * body2->target->getMass() * n * 0.5f;
	sf::Vector2f newV2 = V2 + optimisedP * body1->target->getMass() * n * 0.5f;

	body1->target->setSpeed(newV1);
	body2->target->setSpeed(newV2);
      }
      body2 = body2->next;
    }

    if (body1->target->getCOMPosition().x - body1->target->shape.getRadius() < 0.f){
	body1->target->setSpeed(sf::Vector2f(abs(body1->target->getSpeed().x)+1.f,body1->target->getSpeed().y));
	body1->target->setCOMPosition(sf::Vector2f(body1->target->shape.getRadius(),body1->target->getCOMPosition().y));
    }
    if (body1->target->getCOMPosition().x + body1->target->shape.getRadius() > windowSize.x){
	body1->target->setSpeed(sf::Vector2f(-abs(body1->target->getSpeed().x)-1.f,body1->target->getSpeed().y));
	body1->target->setCOMPosition(sf::Vector2f(windowSize.x - body1->target->shape.getRadius(),body1->target->getCOMPosition().y));
    }
    if (body1->target->getCOMPosition().y - body1->target->shape.getRadius() < 0.f){
	body1->target->setSpeed(sf::Vector2f(body1->target->getSpeed().x,abs(body1->target->getSpeed().y)+1.f));
	body1->target->setCOMPosition(sf::Vector2f(body1->target->getCOMPosition().x, body1->target->shape.getRadius()));
    }
    if (body1->target->getCOMPosition().y + body1->target->shape.getRadius() > windowSize.y){
	body1->target->setSpeed(sf::Vector2f(body1->target->getSpeed().x,-abs(body1->target->getSpeed().y)-1.f));
	body1->target->setCOMPosition(sf::Vector2f(body1->target->getCOMPosition().x, windowSize.y - body1->target->shape.getRadius()));
    }

    body1 = body1->next;
  }
}
