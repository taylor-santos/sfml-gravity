#include "headers.hpp"
#include "Planet.hpp"
#include "powers.hpp"
#include "vectorMath.hpp"

void Planet::AddAcceleration(sf::Vector2f Accel, float frameTime){
  speed += Accel * frameTime;
}
void Planet::Move(float frameTime){
  shape.setPosition(shape.getPosition() + speed * frameTime * pixelsPerMeter);
  float speedMagnitude = magnitude(speed);
  shape.setFillColor(sf::Color(0+speedMagnitude*10,0,255-speedMagnitude*10));
  //mass = speedMagnitude;
}

sf::Vector2f Planet::getSpeed(){
  return speed;
}

void Planet::setSpeed(sf::Vector2f newSpeed){
  speed = newSpeed;
}

float Planet::getMass(){
  return mass;
}

sf::Vector2f Planet::getCOMPosition(){
  return sf::Vector2f(shape.getPosition().x + shape.getRadius(),shape.getPosition().y + shape.getRadius());
}

void Planet::setCOMPosition(sf::Vector2f position){
  shape.setPosition(position.x - shape.getRadius(), position.y - shape.getRadius());
}

Planet::Planet(sf::Color color, float radius, float density, sf::Vector2f position){
  shape.setRadius(radius);
  shape.setPosition(position);
  shape.setFillColor(color);
  float volume = 4.f/3 * 3.14159f * pow(radius / pixelsPerMeter,3);
  mass = volume * density;
}
