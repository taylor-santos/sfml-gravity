#include "headers.hpp"
#include "vectorMath.hpp"
#include "Planet.hpp"
#include "linkedlist.hpp"
#include "powers.hpp"
#include <math.h>

void applyGravity(LinkedList* planetList,float frameTime){
  node* body1 = planetList->first;
  node* body2;
  float x1,x2,y1,y2,M1,M2;
  float G = 6.67384e-11;
  while (body1->next != NULL){
    body2 = body1->next;
    while (body2 != NULL){
      float relativePosition = magnitude(body2->target->getCOMPosition() - body1->target->getCOMPosition());
      float combinedRadius = body1->target->shape.getRadius() + body2->target->shape.getRadius();
      if (relativePosition >= combinedRadius){
	x1 = body1->target->getCOMPosition().x;// / pixelsPerMeter;
	x2 = body2->target->getCOMPosition().x;// / pixelsPerMeter;
	y1 = body1->target->getCOMPosition().y;// / pixelsPerMeter;
	y2 = body2->target->getCOMPosition().y;// / pixelsPerMeter;
	M1 = body1->target->getMass();
	M2 = body2->target->getMass();
	float rSquared = pow(x2 - x1, 2) + pow(y2 - y1, 2);
	sf::Vector2f accel = normalize(body2->target->getCOMPosition() - body1->target->getCOMPosition())*100.f;
	body1->target->AddAcceleration(accel * (M2/rSquared), frameTime);
	body2->target->AddAcceleration(-accel * (M1/rSquared), frameTime);
      }
      body2 = body2->next;
    }
    body1 = body1->next;
  }
}


