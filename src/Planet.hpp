//header file for the planet class

#ifndef PLANET_H
#define PLANET_H

#include "headers.hpp"
#include "powers.hpp"

class LinkedList;

class Planet{   //Planet class. Includes all variables for circular planet.
private:
  sf::Color color;
  float radius;
  float mass;
  sf::Vector2f speed;

public:
  sf::CircleShape shape;
  Planet(sf::Color, float, float, sf::Vector2f);   //Constructor. Takes color, radius, density, and position.
  void AddAcceleration (sf::Vector2f, float);   //Adds a given acceleration to the current velocity multiplied by frame time.
  void Move(float);   //Changes the current position given the current velocity and frame time.
  sf::Vector2f getSpeed(); //Returns speed as a Vector2.
  void setSpeed(sf::Vector2f); //Sets the planet's speed based on given Vector2.
  float getMass();
  sf::Vector2f getCOMPosition(); //Returns Vector2 representing coordinates of the Center of Mass.
  void setCOMPosition(sf::Vector2f);
};

#endif
