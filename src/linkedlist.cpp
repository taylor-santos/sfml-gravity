#include "Planet.hpp"
#include "linkedlist.hpp"
#include <iostream>


LinkedList::LinkedList(){
  count = 0;
  first = NULL;
  last = NULL;
}

void LinkedList::Add(Planet* newPlanet){
  if (count == 0){
    node* newNode = (node*)malloc(sizeof(node));
    newNode->target = newPlanet;
    first = newNode;
    last = newNode;
    newNode->prev = NULL;
    newNode->next = NULL;
    ++count;
  }else
    {
      node* newNode = (node*)malloc(sizeof(node));
      newNode->target = newPlanet;
      newNode->prev = last;
      last->next = newNode;
      last = newNode;
      newNode->next = NULL;
      ++count;
    }
  
}

void LinkedList::Remove(node* nodeToRemove){
    if (nodeToRemove == first){
        first = nodeToRemove->next;
	if (nodeToRemove->next != NULL)
	    nodeToRemove->next->prev = NULL;
    }else if (nodeToRemove == last){
        last = nodeToRemove->prev;
	if (nodeToRemove->prev != NULL)
	    nodeToRemove->prev->next = NULL;
    }else{
        nodeToRemove->prev->next = nodeToRemove->next;
	nodeToRemove->next->prev = nodeToRemove->prev;
    }
    count--;
    delete nodeToRemove;
}

void LinkedList::MoveAll(float frameTime){
  node* curr = first;
  while (curr!= NULL){
    curr->target->Move(frameTime);
    curr = curr->next;
  }
}

void LinkedList::DrawAll(sf::RenderWindow* window){
  node* curr = first;
  while (curr != NULL){
    window->draw(curr->target->shape);
    curr = curr->next;
  }
}
