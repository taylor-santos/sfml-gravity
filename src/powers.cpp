//source file for namespace that contains functions relating to powers
#include "headers.hpp"
#include "powers.hpp"

float pow(float value, int power){ //Basic positive integer exponents.
	float newVal = 1;
	for (int i=0; i<power; ++i){
		newVal *= value;
	}
	return newVal;
}
