//header file for the Doubly Linked List of planets

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "Planet.hpp"

struct node
{
  Planet* target;
  node* prev;
  node* next;
};

class LinkedList
{
public:
  int count;
  LinkedList();
  void Add(Planet*);
  void Remove(node*);
  node* first;
  node* last;
  void MoveAll(float);
  void DrawAll(sf::RenderWindow*);
};


#endif
