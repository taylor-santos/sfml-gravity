#ifndef VECTORMATH_H
#define VECTORMATH_H

float magnitude(sf::Vector2f);

sf::Vector2f normalize(sf::Vector2f);

float dot(sf::Vector2f,sf::Vector2f);

#endif



