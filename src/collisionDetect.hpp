#ifndef COLLISIONDETECT_H
#define COLLISIONDETECT_H

#include "linkedlist.hpp"

void checkCollisions(LinkedList*);

#endif
