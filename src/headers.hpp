#ifndef HEADERS_H
#define HEADERS_H

#include <SFML/Graphics.hpp>
#include <iostream>

const float pixelsPerMeter = 10;
const sf::Vector2f windowSize = sf::Vector2f(1280.f,720.f);

#endif
