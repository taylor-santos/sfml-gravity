#include "headers.hpp"
#include <math.h>

float magnitude(sf::Vector2f vector){
  return (float)sqrt(pow(vector.x,2)+pow(vector.y,2));
}

sf::Vector2f normalize(sf::Vector2f vector){
  float magnitude = (float)sqrt(vector.x*vector.x + vector.y*vector.y);
  return sf::Vector2f(vector.x/magnitude,vector.y/magnitude);
}

float dot(sf::Vector2f v1,sf::Vector2f v2){
  return (v1.x * v2.x + v1.y * v2.y);
}
